<?php
add_action('admin_menu', 'add_submenu');

function add_submenu(){
    $parent_slug = "gestion_annuaire";
    $page_title = "index.php/annuaire_backoffice/ajouter";
    $menu_title ="Ajouter une entreprise";
    $capability = "administrator";
    $menu_slug="ajouter_entreprise";
    $form_annuaire = function () {
    ?>
        </br>
        <h1 style="text-align: center;">Ajouter une entreprise à l'annuaire</h1>
        
        <form action="/wp-admin/admin.php?page=ajouter_entreprise" method="POST" style="display:flex; flex-direction:column; align-items : center; justify-content:center;">
            <label for="nom_entreprise">Nom entreprise</label>
            <input type="text" name="nom_entreprise">
            
            <label for="localisation_entreprise">Localisation entreprise</label>
            <input type="text" name="localisation_entreprise">
            
            <label for="prenom_contact">Prénom du contact</label>
            <input type="text" name="prenom_contact">
            
            <label for="nom_contact">Nom du contact</label>
            <input type="text" name="nom_contact">
            
            <label for="mail_contact">Mail du contact</label>
            <input type="text" name="mail_contact">
            
            <button type="submit">Envoyer</button>
        </form>
    <?php    
    if ($_POST){
        global $wpdb;
        $table_name = $wpdb->prefix . "annuaire_entreprises";
        $nom_entreprise = $_POST['nom_entreprise'];
        $localisation_entreprise = $_POST['localisation_entreprise'];
        $prenom_contact = $_POST['prenom_contact'];
        $nom_contact = $_POST['nom_contact'];
        $mail_contact = $_POST['mail_contact'];
        $wpdb->insert($table_name, array(
            'nom_entreprise' =>  $nom_entreprise,
            'localisation_entreprise' => $localisation_entreprise,
            'prenom_contact' => $prenom_contact,
            'nom_contact' => $nom_contact,
            'mail_contact' => $mail_contact,
        ));
        echo "</br><p>L'entreprise <strong>{$nom_entreprise}</strong> à {$localisation_entreprise} avec {$prenom_contact} {$nom_contact} comme contact a bien été ajoutée à la base de données !</p>";
    }
    };
    add_submenu_page( $parent_slug, $page_title, $menu_title, $capability, $menu_slug, $form_annuaire, $position = null );
}