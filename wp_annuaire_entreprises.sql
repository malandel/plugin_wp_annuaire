-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le : sam. 03 avr. 2021 à 10:49
-- Version du serveur :  8.0.23-0ubuntu0.20.04.1
-- Version de PHP : 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `wp_annuaire`
--

-- --------------------------------------------------------

--
-- Structure de la table `wp_annuaire_entreprises`
--

CREATE TABLE `wp_annuaire_entreprises` (
  `id` int NOT NULL,
  `nom_entreprise` varchar(100) NOT NULL,
  `localisation_entreprise` varchar(100) NOT NULL,
  `prenom_contact` varchar(100) NOT NULL,
  `nom_contact` varchar(100) NOT NULL,
  `mail_contact` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `wp_annuaire_entreprises`
--

INSERT INTO `wp_annuaire_entreprises` (`id`, `nom_entreprise`, `localisation_entreprise`, `prenom_contact`, `nom_contact`, `mail_contact`) VALUES
(1, 'Supernova', 'Toulouse', 'Marcel', 'Rodriguez', 'contact@rodrigo.com'),
(2, 'IndieTroll', 'Carbonne', 'Michel', 'Tournier', 'lastar@planetaire.org'),
(3, 'Laboulette', 'Saint Gaudens', 'Julie', 'Lila', 'lilali@lololu.net'),
(4, 'Simplon', 'Labège', 'Aurélien', 'Chirot', 'achirot@simplon.co'),
(5, 'Simplon', 'Labège', 'Raphaël', 'Apard', 'rapard@simplon.co'),
(6, 'Simplon', 'Labège', 'Nicolas', 'Piquet', 'npiquet@simplon.co'),
(9, 'Adacraft', 'Toulouse', 'Nicolas', 'Decoster', 'n.decoster@gmail.com'),
(10, 'Fabernovel', 'Toulouse', 'Julien ', 'Mession', 'julien.mession@fabernovel.com');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `wp_annuaire_entreprises`
--
ALTER TABLE `wp_annuaire_entreprises`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `wp_annuaire_entreprises`
--
ALTER TABLE `wp_annuaire_entreprises`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
