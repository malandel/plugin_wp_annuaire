<?php

/**
 * Add a page to the backoffice menu.
 */

add_action('admin_menu', 'add_page');

function add_page()
{
    $page_title = "index.php/annuaire_backoffice";
    $menu_title = "Gestion Annuaire";
    $capability = "administrator";
    $menu_slug = "gestion_annuaire";
    $homepage = function (){
?>
    </br>
    <div style="text-align: center;">
         <h1>Bienvenue sur l'interface de gestion de votre annuaire</h1>
         <p>Ajoutez, supprimez ou modifiez les informations contenues dans votre base de données.</p>
    </div>
    <div style="margin-top: 30px;">
        <ol>
<?php
    global $wpdb;
    $table_name = $wpdb-> prefix . 'annuaire_entreprises';
    $data = $wpdb->get_results("SELECT * FROM $table_name");
    foreach($data as $row){
?>
            <li>
                <strong><?=$row->nom_entreprise;?></strong> à <?=$row->localisation_entreprise;?> : 
                </br><?=$row->prenom_contact;?> <?=$row->nom_contact;?> - <?=$row->mail_contact;?>
            </li>
<?php
    }
?>
        </ol>
       </div>
       <?php
    };
   

    add_menu_page($page_title, $menu_title, $capability, $menu_slug, $homepage);
}
