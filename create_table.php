<?php

// When the plugin is activated, it executes several functions : 
//It creates the table 'annuaire_entreprises'
register_activation_hook( __FILE__, 'create_table' );
//It adds some predefined data into the table
register_activation_hook( __FILE__, 'populate_table' );

function create_table(){
    global $wpdb;
    $table_name = $wpdb->prefix . 'annuaire_entreprises';
    $sql = "CREATE TABLE $table_name (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `nom_entreprise` varchar(100) NOT NULL,
        `localisation_entreprise` varchar(100) NOT NULL,
        `prenom_contact` varchar(100) NOT NULL,
        `nom_contact` varchar(100) NOT NULL,
        `mail_contact` varchar(100) NOT NULL,
        PRIMARY KEY  (id)
      ) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
    
    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $sql );
}

function populate_table(){
    global $wpdb;
    $table_name = $wpdb-> prefix . 'annuaire_entreprises';
    //The data we will insert into the table
    $data = array( 
        ['Supernova', 'Toulouse', 'Marcel', 'Rodriguez', 'contact@rodrigo.com'],
        ['IndieTroll', 'Carbonne', 'Michel', 'Tournier', 'lastar@planetaire.org'],
        ['Laboulette', 'Saint Gaudens', 'Julie', 'Lila', 'lilali@lololu.net']
    );
    foreach ($data as $row){
        $wpdb->insert( 
            $table_name, 
            array( 
                'nom_entreprise' =>  $row[0], 
                'localisation_entreprise' => $row[1], 
                'prenom_contact' => $row[2], 
                'nom_contact' => $row[3], 
                'mail_contact' => $row[4],
            ),
        );
    }
}