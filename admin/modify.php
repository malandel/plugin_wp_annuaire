<?php
add_action('admin_menu', 'add_submenu_modify');

function add_submenu_modify(){
    $parent_slug = "gestion_annuaire";
    $page_title = "index.php/annuaire_backoffice/modifier";
    $menu_title ="Modifier une entreprise";
    $capability = "administrator";
    $menu_slug="modifier_entreprise";
    $form_annuaire = function () {
        global $wpdb;
        $table_name = $wpdb-> prefix . 'annuaire_entreprises';
        $data = $wpdb->get_results("SELECT * FROM $table_name");
?>
        </br>
        <h1 style="text-align: center;">Modifier une entreprise de l'annuaire</h1>
        
        <form action="/wp-admin/admin.php?page=modifier_entreprise" method="POST" style="display:flex; flex-direction:column; align-items : center; justify-content:center;">
            <select name="choose">
                <option value="">--Choisir une entreprise à modifier--</option>
<?php
    foreach($data as $row){
?>
                <option value="<?= $row->id;?>"> <?=$row->id;?> - <strong><?=$row->nom_entreprise;?></strong> à <?=$row->localisation_entreprise;?> - <?=$row->prenom_contact;?> <?=$row->nom_contact;?> - <?=$row->mail_contact;?></option>
<?php
    }
?>
            </select>
            <button type="submit">Modifier</button>
        </form>
    <?php
        $id = $_POST["choose"];
        $data_modify = $wpdb->get_row("SELECT * FROM $table_name WHERE id = $id");
?>
        </br>        
        <form action="/wp-admin/admin.php?page=modifier_entreprise" method="POST" style="display:flex; flex-direction:column; align-items : center; justify-content:center;">
            <label for="nom_entreprise">Nom entreprise</label>
            <input type="text" name="nom_entreprise" value="<?= $data_modify->nom_entreprise;?>">
            
            <label for="localisation_entreprise">Localisation entreprise</label>
            <input type="text" name="localisation_entreprise" value="<?= $data_modify->localisation_entreprise;?>">
            
            <label for="prenom_contact">Prénom du contact</label>
            <input type="text" name="prenom_contact" value="<?= $data_modify->prenom_contact;?>">
            
            <label for="nom_contact">Nom du contact</label>
            <input type="text" name="nom_contact" value="<?= $data_modify->nom_contact;?>">
            
            <label for="mail_contact">Mail du contact</label>
            <input type="text" name="mail_contact" value="<?= $data_modify->mail_contact;?>">

            <input type="hidden" name="id" value="<?=$id;?>">
            
            <button type="submit">Envoyer</button>
        </form>
    <?php    
        if ($_POST["id"]){

            $nom_entreprise = $_POST['nom_entreprise'];
            $localisation_entreprise = $_POST['localisation_entreprise'];
            $prenom_contact = $_POST['prenom_contact'];
            $nom_contact = $_POST['nom_contact'];
            $mail_contact = $_POST['mail_contact'];
            $id = $_POST["id"];
            $wpdb->update($table_name, 
            array(
                'nom_entreprise' =>  $nom_entreprise,
                'localisation_entreprise' => $localisation_entreprise,
                'prenom_contact' => $prenom_contact,
                'nom_contact' => $nom_contact,
                'mail_contact' => $mail_contact,
            ),
            array(
                'id' => $id,
            ),
        );
        echo "<p>L'entreprise <strong>{$nom_entreprise}</strong> à {$localisation_entreprise} avec {$prenom_contact} {$nom_contact} comme contact a bien été modifiée !</p>";
    }
};
    add_submenu_page( $parent_slug, $page_title, $menu_title, $capability, $menu_slug, $form_annuaire);
}