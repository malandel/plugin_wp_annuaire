<?php
add_action('admin_menu', 'add_submenu_delete');

function add_submenu_delete(){
    $parent_slug = "gestion_annuaire";
    $page_title = "index.php/annuaire_backoffice/supprimer";
    $menu_title ="Supprimer une entreprise";
    $capability = "administrator";
    $menu_slug="supprimer_entreprise";
    $form_annuaire = function () {
        global $wpdb;
        $table_name = $wpdb-> prefix . 'annuaire_entreprises';
        $data = $wpdb->get_results("SELECT * FROM $table_name");
    ?>
        </br>
        <h1 style="text-align: center;">Supprimer une entreprise de l'annuaire</h1>
        
        <form action="/wp-admin/admin.php?page=supprimer_entreprise" method="POST" style="display:flex; flex-direction:column; align-items : center; justify-content:center;">
            <select name="entreprise">
                <option value="">--Choisir une entreprise à supprimer--</option>
<?php
    foreach($data as $row){

?>
                <option value="<?= $row->id;?>"><?=$row->id;?> - <strong><?=$row->nom_entreprise;?></strong> à <?=$row->localisation_entreprise;?> - <?=$row->prenom_contact;?> <?=$row->nom_contact;?> - <?=$row->mail_contact;?></option>
<?php
    }
?>
            </select>
            <button type="submit">Supprimer</button>
        </form>
    <?php    
    if ($_POST){
        global $wpdb;
        $table_name = $wpdb->prefix . "annuaire_entreprises";
        $id = $_POST['entreprise'];
        $wpdb->delete($table_name, array(
            'id' =>  $id,
        ));
        echo "</br><p>L'entreprise <strong>{$id}</strong> a bien été supprimée de la base de données !</p>";
    }
    };
    add_submenu_page( $parent_slug, $page_title, $menu_title, $capability, $menu_slug, $form_annuaire );
}