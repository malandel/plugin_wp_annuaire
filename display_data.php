<?php

// When the plugin is activated, it executes several functions : 
//It creates the shortcode [data] that executes the function display_data()
//display_data() retrieves all the data from annuaire_entreprises and display it into an HTML table
register_activation_hook( __FILE__, add_shortcode( 'data', 'display_data' ));

function display_data()
{
  global $wpdb;
  $table_name = $wpdb-> prefix . 'annuaire_entreprises';
  $data = $wpdb->get_results("SELECT * FROM $table_name");
  echo "<table>
          <thead>
            <tr style='color:#ff7ba1;'>
              <th>Nom entreprise</th>
              <th>Localisation entreprise</th>
              <th>Prénom contact</th>
              <th>Nom contact</th>
              <th>Mail contact</th>
            </tr>
          </thead>
            ";
  foreach($data as $row){
    $colors = [' #7993f1 ', ' #7ef179 ', ' #9379f1 ', ' #d860f6 ', ' #f66074 ', ' #60bbf6 ', ' #eead49 ', ' #e9ee49 ', ' #32c218 ', ' #a56a76 '];
    echo "<tbody>
            <tr style='color:{$colors[random_int(0, count($colors))]};'>
              <td><strong>{$row->nom_entreprise}</strong></td>
              <td>{$row->localisation_entreprise}</td>
              <td>{$row->prenom_contact}</td>
              <td>{$row->nom_contact}</td>
              <td>{$row->mail_contact}</td>
            </tr>              
          </tbody>
        ";
  }
  echo "</table>";
}